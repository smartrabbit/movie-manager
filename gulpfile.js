var gulp = require('gulp');
var dest = require('gulp-dest');
var sass = require('gulp-ruby-sass');
var minifyCss = require('gulp-minify-css');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish-ex');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");

gulp.task('default', function() {
    // place code for your default task here
    gulp.watch('assets/styles/**/*.scss', ['estilos']);
    gulp.watch('assets/scripts/**/*.js', ['scripts']);
});

gulp.task('lint', function() {
    var source = [
        './assets/scripts/*.js'
    ];
    return gulp.src( source )
        .pipe( jshint( '.jshintrc' ) )
        .pipe( jshint.reporter( stylish ) );
});
gulp.task('compress', function() {
    return gulp.src('./assets/scripts/**/*.js')
        .pipe(uglify({mangle: false}))
        .pipe(rename(function (path) {
            path.extname = ".min.js"
        }))
        .pipe(gulp.dest('./js'));
});

gulp.task('sass', function () {
    return sass('./assets/styles/**/*.scss')
        .on('error', sass.logError)
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(rename(function (path) {
            path.extname = ".min.css"
        }))
        .pipe(gulp.dest('./styles'))
});


gulp.task('estilos', ['sass']);
//gulp.task('scripts', ['lint', 'compress']);
gulp.task('scripts', ['compress']);