(function () {
    'use strict';

    var services = function ($http, $timeout, $location) {
        //Obtener todas las colecciones actuales

        var getMovies = function () {
            var lib = new localStorageDB("library", localStorage);
            if( !lib.tableExists("movies") ) {
                return $http.get("movies.json").success(function (data, status, headers, config) {

                    if (typeof(Storage) !== "undefined") {

                        if (!lib.tableExists("movies")) {

                            lib.createTable("movies", ["name", "release_year", "gross_income", "actors", "director_name", "rating", "genre", "picture"]);
                            //lib.createTable("actors", ["name", "title", "author", "year", "copies"]);
                            var moviesdata = JSON.stringify(data);
                            var movies = JSON.parse(moviesdata);
                            for (var i = 0; i < movies.length; i++) {
                                lib.insert("movies", {
                                    name: movies[i].Name,
                                    release_year: movies[i].release_year,
                                    gross_income: movies[i].gross_income,
                                    actors: movies[i].actors,
                                    rating: movies[i].rating,
                                    genre: movies[i].genre,
                                    picture: movies[i].Picture

                                });
                            }
                            //
                            lib.commit();

                        }


                    } else {
                        // Sorry! No Web Storage support..
                        console.log("Your browser no support local storage")
                    }

                }).then(
                    function (response) {
                        //This timeout is used to simulate a slow download time

                        var result = lib.queryAll("movies");

                        return $timeout(function () {
                            return result;
                        }, 100);

                    });
            }
        };
        var getActors = function () {
            var lib = new localStorageDB("library", localStorage);
            return $http.get("actors.json").success(function (data, status, headers, config) {
                if(typeof(Storage) !== "undefined") {

                    if(!lib.tableExists("actors") ) {
                        lib.createTable("actors", ["name", "last_name","birth","gender"]);
                        //lib.createTable("actors", ["name", "title", "author", "year", "copies"]);
                        var actorsdata=JSON.stringify(data);
                        var actors=JSON.parse(actorsdata);
                        for (var i = 0; i < actors.length; i++){
                            lib.insert("actors", {
                                name:actors[i].First_name,
                                last_name:actors[i].Last_name,
                                gender:actors[i].gender,
                                birth:actors[i].birth_date
                            });
                        }
                        //
                        lib.commit();
                    }


                } else {
                    // Sorry! No Web Storage support..
                    console.log("Your browser no support local storage")
                }

            }).then(
                function (response) {
                    //This timeout is used to simulate a slow download time

                    var result=lib.queryAll("actors");

                    return $timeout(function () {
                        return result;
                    }, 100);

                });
        };
        var getMoviesSearch = function (moviename) {
            var lib = new localStorageDB("library", localStorage);
            return $http.get("movies.json").success(function (data, status, headers, config) {
                if(typeof(Storage) !== "undefined") {

                    if( !lib.tableExists("movies") ) {

                        lib.createTable("movies", ["name", "release_year", "gross_income", "actors","director_name", "rating","genre","picture"]);
                        //lib.createTable("actors", ["name", "title", "author", "year", "copies"]);
                        var moviesdata=JSON.stringify(data);
                        var movies=JSON.parse(moviesdata);
                        for (var i = 0; i < movies.length; i++){
                            lib.insert("movies", {
                                name:movies[i].Name,
                                release_year:movies[i].release_year,
                                gross_income:movies[i].gross_income,
                                actors:movies[i].actors,
                                rating:movies[i].rating,
                                genre:movies[i].genre,
                                picture:movies[i].Picture

                            });
                        }
                        //
                        lib.commit();
                    }
                } else {
                    // Sorry! No Web Storage support..
                    console.log("Your browser no support local storage")
                }

            }).then(
                function (response) {
                    //This timeout is used to simulate a slow download time


                    //Search moviename;
                    var result=lib.queryAll("movies",{
                        query: function(row) {
                            if(row.name.toLowerCase().indexOf(moviename.toLowerCase()) > -1 ) {
                                return true;
                            } else {
                                return false;
                            }
                        },
                    });

                    return $timeout(function () {
                        return result;
                    }, 100);

                });
        };
        return {
            getMovies: getMovies,
            getActors: getActors,
            getMoviesSearch:getMoviesSearch
        };

    };

    var module = angular.module("movieApp");
    module.config(['$httpProvider', function($httpProvider) {
        //initialize get if not there
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }

        // Answer edited to include suggestions from comments
        // because previous version of code introduced browser-related errors

        //disable IE ajax request caching
        $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
        // extra
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    }]);

    module.factory("services", services);

}());