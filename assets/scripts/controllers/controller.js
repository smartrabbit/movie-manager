(function () {
    var movieApp=  angular.module('movieApp.controllers', []);

    movieApp.controller('MovieController', function (services,$rootScope, $scope, $routeParams) {

        var onResult = function (data) {
            $scope.MovieList=data;
        };
        var onError = function (reason) {
            console.log("error loading data")
        };
        var lib = new localStorageDB("library", localStorage)
        if (lib.tableExists("movies")) {
            var data = lib.queryAll("movies");
            $scope.MovieList=data;
        }else {
            services.getMovies().then(onResult, onError);
        }

    });
    movieApp.controller('MainController', function (services,$rootScope, $scope, $routeParams,$location) {

    $scope.searchBtn=function(){

        var search= $scope.search;

        $location.path( "/home/"+search );

    }


    });
    movieApp.controller('SearchController', function (services,$rootScope, $scope, $routeParams) {
            var onResult = function (data) {
                $scope.MovieList=data;
            };
            var onError = function (reason) {
                console.log("error loading data")
            };
            var search= $routeParams.param;
            $scope.search= search;

            var lib = new localStorageDB("library", localStorage)
            if (lib.tableExists("movies")) {

                var data=lib.queryAll("movies",{
                    query: function(row) {
                        if(row.name.toLowerCase().indexOf(search.toLowerCase()) > -1 ) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                });
                $scope.MovieList=data;

            }else {
                services.getMoviesSearch(search).then(onResult, onError);
            }

    });
    //controller Of the Movie Information View
    movieApp.controller('MovieInfoController', function($rootScope,$scope, $routeParams, $location) {

        var idmovie = $routeParams.param;
        var lib = new localStorageDB("library", localStorage)

        var movie=lib.queryAll("movies", {
            query: {ID: idmovie},limit:1
        });
        if(movie.length>0){
            $scope.movieexist=true;
            $scope.name=movie[0].name;
            $scope.release_year=movie[0].release_year;
            $scope.director_name=movie[0].director_name;
            $scope.picture=movie[0].picture;
            $scope.gross_income=movie[0].gross_income;
            $scope.Genre=movie[0].genre;
            $scope.MovieActors=movie[0].actors;
            $scope.rating=movie[0].rating;
            $scope.ID=movie[0].ID;



        }else{

            $scope.movieexist=false;
        }

        $scope.delete=function(ID){
            if (window.confirm("Do you really want to delete this movie?")) {
               // var lib = new localStorageDB("library", localStorage);
                lib.deleteRows("movies", {ID: ID});
                if(lib.commit()){
                    //$location.path( "/home" );
                    $location.url("/");
                }else{
                    alert("Unable to delete the movie. Try again later")
                }

            }
        };
    });

    //Controller to Creat a new Movie
    movieApp.controller('CreateMovieController', function(services,$rootScope,$scope, $routeParams, $location) {


        var onResult = function (data) {

            $scope.Actors=data;
        };

        var onError = function (reason) {
            console.log("error loading data")
        };
        services.getActors().then(onResult, onError);

        $scope.saveMovie=function(){
            var lib = new localStorageDB("library", localStorage)
            lib.insert("movies",{
                "name" : $scope.name,
                "release_year":$scope.release_year,
                "gross_income":$scope.gross_income,
                "actors": $scope.MovieActors,
                "director_name":$scope.director_name,
                "picture":$scope.picture,
                "rating":0,
                "genre":$scope.Genre});

            if(lib.commit()){
                alert("Movie Saved!");
                $location.path( "/home" );

            }else{
                alert("There is a problema creating the Movie");

            }

        }


        //Save the json in the local storage movies variable

    });

    //Controller to edit a Movie
    movieApp.controller('EditMovieController', function(services,$rootScope,$scope, $routeParams, $location) {

        var lib = new localStorageDB("library", localStorage)
        if(lib.tableExists("movies") ) {

            var onResult = function (data) {
                $scope.Actors = data;
//Get the information of that movie
                var idmovie = $routeParams.param;
                var movie = lib.queryAll("movies", {
                    query: {ID: idmovie}, limit: 1
                });
                if (movie) {
                    $scope.name = movie[0].name;
                    $scope.release_year = movie[0].release_year;
                    $scope.director_name = movie[0].director_name;
                    $scope.picture = movie[0].picture;
                    $scope.gross_income = movie[0].gross_income;
                    $scope.Genre = movie[0].genre;
                    var tempData = [];

                    var parsed = $scope.Actors;
                    var parsed2 = movie[0].actors
                    var arr = [];
                    var arr2 = [];
                    var result=[];
                    for(var x in parsed){

                        arr.push(parsed[x]);

                    }
                    for(var y in parsed2){
                        arr2.push(parsed2[y]);
                    }

                    for(var i=0;i<arr.length;i++){
                        for(var j=0;j<arr2.length;j++){

                            if(arr[i].ID==arr2[j].ID){

                                result.push(arr[i]);
                            }
                        }
                    }
                    $scope.MovieActors =result;
                    $scope.ID = movie[0].ID;
                    $scope.rating = movie[0].rating;
                } else {
                    $scope.movieexist = false;
                }
            };

            var onError = function (reason) {
                console.log("error loading data")
            };

            if(lib.tableExists("actors") ) {
                $scope.Actors =lib.queryAll("actors");
                //Get the information of that movie
                var idmovie = $routeParams.param;
                var movie = lib.queryAll("movies", {
                    query: {ID: idmovie}, limit: 1
                });
                if (movie) {
                    $scope.name = movie[0].name;
                    $scope.release_year = movie[0].release_year;
                    $scope.director_name = movie[0].director_name;
                    $scope.picture = movie[0].picture;
                    $scope.gross_income = movie[0].gross_income;
                    $scope.Genre = movie[0].genre;
                    var tempData = [];

                    var parsed = $scope.Actors;
                    var parsed2 = movie[0].actors
                    var arr = [];
                    var arr2 = [];
                    var result=[];
                    for(var x in parsed){

                        arr.push(parsed[x]);

                    }
                    for(var y in parsed2){
                        arr2.push(parsed2[y]);
                    }

                    for(var i=0;i<arr.length;i++){
                        for(var j=0;j<arr2.length;j++){

                            if(arr[i].ID==arr2[j].ID){

                                result.push(arr[i]);
                            }
                        }
                    }
                    $scope.MovieActors =result;
                    $scope.ID = movie[0].ID;
                    $scope.rating = movie[0].rating;
                } else {
                    $scope.movieexist = false;
                }

            }else{
                services.getActors().then(onResult, onError);
            }


            $scope.updateMovie = function (ID) {

                var lib = new localStorageDB("library", localStorage)
                if (ID) {
                    lib.update("movies", {ID: ID}, function (row) {

                        row.name = $scope.name;
                        row.release_year = $scope.release_year;
                        row.actors = $scope.MovieActors,
                            row.director_name = $scope.director_name;
                        row.picture = $scope.picture;
                        row.rating = $scope.rating;
                        row.genre = $scope.Genre;
                        row.gross_income = $scope.gross_income;


                        // the update callback function returns to the modified record
                        return row;
                    });
                    lib.commit();
                    alert("Movie Updated");
                    $location.path( "/movie/"+ID );

                } else {
                    alert("You can't update this movie")
                }
            }

        }else{

            $location.path( "/home" );
        }
    });

    movieApp.controller('ActorInfoController', function($rootScope,$scope, $routeParams, $location) {
        var lib = new localStorageDB("library", localStorage)
        var idactor = $routeParams.param;
        var actor=lib.queryAll("actors", {
            query: {ID: idactor},limit:1
        });
        if(actor){
            $scope.movieexist=true;
            $scope.name=actor[0].name;
            $scope.last_name=actor[0].last_name;
            $scope.gender=actor[0].gender;
            $scope.birthday=actor[0].birth;
            $scope.ID=actor[0].ID;

            //Get the list of movie in where the actor appear

        }else{

            $scope.movieexist=false;
        }

    });

    movieApp.controller('CreateActorController', function(services,$rootScope,$scope, $routeParams, $location) {



        $scope.saveActor=function(){
            var lib = new localStorageDB("library", localStorage)
            lib.insert("actors", {
                "name": $scope.name,
                "last_name": $scope.last_name,
                "birth": $scope.birth,
                "gender": $scope.gender,
            });
            lib.commit();

        }


        //Save the json in the local storage movies variable

    });

    movieApp.controller('EditActorController', function(services,$rootScope,$scope, $routeParams, $location) {

        var lib = new localStorageDB("library", localStorage)
        var idactor = $routeParams.param;
        var actor=lib.queryAll("actors", {
            query: {ID: idactor},limit:1
        });
        if(actor){
            $scope.movieexist=true;
            $scope.name=actor[0].name;
            $scope.last_name=actor[0].last_name;
            $scope.gender=actor[0].gender;
            $scope.birthday=actor[0].birth;
            $scope.ID=actor[0].ID;

            //Get the list of movie in where the actor appear

        }else{

            $scope.movieexist=false;
        }
        $scope.updateActor = function (ID) {

            var lib = new localStorageDB("library", localStorage)
            if (ID) {
                lib.update("actors", {ID: ID}, function (row) {

                    row.name = $scope.name;
                    row.last_name = $scope.last_name;
                    row.gender = $scope.gender,
                    row.birth= $scope.birthday;



                    // the update callback function returns to the modified record
                    return row;
                });
                lib.commit();
                alert("Actor Updated");
                $location.path( "/actor/"+ID );

            } else {
                alert("You can't update this actor")
            }
        }
    });
} ());

