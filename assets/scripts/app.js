var movieApp=angular.module('movieApp', ['ngRoute','movieApp.controllers']);

movieApp.run(function ($rootScope, $location,$http) {

});
movieApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/movie/:param', {
                templateUrl: 'partials/movie-info.html',
                controller  : 'MovieInfoController'
            }). when('/home', {
                templateUrl: 'partials/home.html',
                controller  : 'MovieController'
            }).
            when('/home/:param', {
                templateUrl: 'partials/home.html',
                controller  : 'SearchController'
            }).
            when('/editmovie/:param', {
                templateUrl: 'partials/movie-edit.html',
                controller  : 'EditMovieController'
            }).
            when('/create', {
                templateUrl: 'partials/movie-create.html',
                controller  : 'CreateMovieController'
            }).
            when('/createActor', {
                templateUrl: 'partials/actor-create.html',
                controller  : 'CreateActorController'
            }).
            when('/actor/:param', {
                templateUrl: 'partials/actor-info.html',
                controller  : 'ActorInfoController'
            }).
            when('/actoredit/:param', {
                templateUrl: 'partials/actor-edit.html',
                controller  : 'EditActorController'
            }).
            otherwise({
                templateUrl: 'partials/home.html',
                controller  : 'MovieController'
            });
    }]);
